const Hapi = require('@hapi/hapi');

const init = async () => {
  const server = Hapi.server({
    port: (+process.env.PORT, '0.0.0.0'),
    host: 'host'
  });

  server.route({
    method: 'GET',
    path: '/hello',
    handler: () => 'Hello World!',
  });

  server.route({
    method: 'GET',
    path: '/',
    handler: () => 'EMO',
  });

  await server.start();
  console.log('Server running on: port %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();
